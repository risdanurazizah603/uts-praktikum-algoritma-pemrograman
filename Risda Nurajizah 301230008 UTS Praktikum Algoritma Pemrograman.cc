#include <iostream>
#include <stdlib.h>

using namespace std;

int main ()
{
    system("clear");
    
    int a = 0;
    int b = 0;
    char OperatorMatematika;
    
    cout << "Masukkan bilangan ke 1 : ";
    cin >> a;
    cout << "Masukkan operator (+,-,*,/) : ";
    cin >> OperatorMatematika;
    cout << "Masukkan bilangan ke 2 : ";
    cin >> b;
    
    switch (OperatorMatematika){
        case '+':
        cout << "Bilangan ke 1 + Bilangan ke 2 = "<<a<<" + "<<b<<" = " <<a+b<< endl;
        break;
        case '-':
         cout << "Bilangan ke 1 - Bilangan ke 2 = "<<a<<" - "<<b<<" = " <<a-b<< endl;
        break;
        case '*':
         cout << "Bilangan ke 1 * Bilangan ke 2 = "<<a<<" * "<<b<<" = " <<a*b<< endl;
        break;
        case '/':
         cout << "Bilangan ke 1 / Bilangan ke 2 = "<<a<<" / "<<b<<" = " <<a/b<< endl;
        break;
        default:
        cout << "Operator tidak valid";
    }
    return 0;
    
}
