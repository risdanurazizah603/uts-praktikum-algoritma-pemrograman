//IF_Quiz_06_12_2023

#include <iostream>
#include <stdlib.h>

using namespace std;

    int combination(int n , int r){
        if (r == 0 || r == n) {
            return 1;
        }else{
            return combination(n-1,r-1)+combination(n-1,r);
        }
    }

int main()
{
    int N;
    cout<<"Masukkan N : ";
    cin>>N;
        for(int a = 0; a < N; a++){
        for(int b = 0; b < N - a - 1; b++){
        cout<<" ";
        }
        for(int b = 0; b <= a;b++){
        cout<<combination(a,b)<<" ";
        }
        cout<<endl;
    }
    return 0;
}
